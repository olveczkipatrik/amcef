### Start
```
pyton app.py
```

### Search for a post by ID
```
curl "http://127.0.0.1:5000/post?id={postID}"
```

### Search for a post by userID
```
curl "http://127.0.0.1:5000/post/user?id=userID"
```

### Add a post
```
curl -X POST "http://127.0.0.1:5000/post" -H "Content-Type: application/json" -d '{"userId": {userID}, "title": "{Title}", "body": "{Body}"}'
```

### Edit a post
```
curl -X PUT "http://127.0.0.1:5000/post/{postID}" -H "Content-Type: application/json" -d '{"title": "{Title}", "body": "{Body}"}'
```

### Delete a post
```
curl -X DELETE "http://127.0.0.1:5000/post/{postID}"
```

# Swagger
```
http://127.0.0.1:5000/apidocs/
```
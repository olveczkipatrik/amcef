from flask import Flask, request, jsonify
from flasgger import Swagger
import json
import requests
import os

app = Flask(__name__)
swagger = Swagger(app)

POSTS_FILE = 'posts.json'

# LOCAL
def read_posts():
    if os.path.exists(POSTS_FILE):
        with open(POSTS_FILE, 'r') as file:
            return json.load(file)
    return []

def write_posts(posts):
    with open(POSTS_FILE, 'w') as file:
        json.dump(posts, file, indent=4)

def find_post_by_id(post_id):
    posts = read_posts()
    for post in posts:
        if post['id'] == post_id:
            return post
    return None

def find_post_by_user_id(user_id):
    posts = read_posts()
    user_posts = [post for post in posts if post['userId'] == user_id]
    return user_posts if user_posts else None

# EXTERNAL
def fetch_post_from_api(post_id):
    response = requests.get(f'https://jsonplaceholder.typicode.com/posts/{post_id}')
    if response.status_code == 200:
        return response.json()
    return None

def validate_user(user_id):
    response = requests.get(f'https://jsonplaceholder.typicode.com/users/{user_id}')
    return response.status_code == 200

@app.route('/post', methods=['GET'])
def get_post_by_id():
    """
    Search for a post by ID
    ---
    parameters:
      - name: id
        in: query
        type: integer
        required: true
        description: The ID of the post to retrieve
    responses:
      200:
        description: Post found
      400:
        description: No ID provided
      404:
        description: Post not found
    """
    post_id = request.args.get('id', type=int)
    if not post_id:
        return jsonify({'error': 'No ID provided'}), 400

    # Local ID
    post = find_post_by_id(post_id)
    if post:
        return jsonify(post), 200

    # External ID
    post = fetch_post_from_api(post_id)
    if post:
        posts = read_posts()
        posts.append(post)
        write_posts(posts)
        return jsonify(post), 200
    else:
        return jsonify({'error': 'Post not found'}), 404

@app.route('/post/user', methods=['GET'])
def get_posts_by_user_id():
    """
    Search for a post by user ID
    ---
    parameters:
      - name: id
        in: query
        type: integer
        required: true
        description: The ID of the user who created the post
    responses:
      200:
        description: Posts found
      400:
        description: No userID provided
      404:
        description: Posts not found for the given userID
    """
    user_id = request.args.get('id', type=int)
    if not user_id:
        return jsonify({'error': 'No userID provided'}), 400

    # Local userID
    user_posts = find_post_by_user_id(user_id)
    if user_posts:
        return jsonify(user_posts), 200

    # External userID
    response = requests.get('https://jsonplaceholder.typicode.com/posts')
    if response.status_code == 200:
        posts = response.json()
        user_posts = [post for post in posts if post['userId'] == user_id]
        if user_posts:
            existing_posts = read_posts()
            existing_posts.extend(user_posts)
            write_posts(existing_posts)
            return jsonify(user_posts), 200
    return jsonify({'error': 'Posts not found for the given userID'}), 404

@app.route('/post', methods=['POST'])
def add_post():
    """
    Add a new post
    ---
    parameters:
      - name: body
        in: body
        required: true
        schema:
          type: object
          required:
            - userId
            - title
            - body
          properties:
            userId:
              type: integer
            title:
              type: string
            body:
              type: string
    responses:
      201:
        description: Post created successfully
      400:
        description: Invalid userId or missing required fields
    """
    data = request.json
    user_id = data.get('userId')
    title = data.get('title')
    body = data.get('body')

    if not user_id or not title or not body:
        return jsonify({'error': 'userId, title, and body are required'}), 400

    if not validate_user(user_id):
        return jsonify({'error': 'Invalid userId'}), 400

    posts = read_posts()
    new_id = max(post['id'] for post in posts) + 1 if posts else 1
    new_post = {
        'userId': user_id,
        'id': new_id,
        'title': title,
        'body': body
    }
    posts.append(new_post)
    write_posts(posts)
    return jsonify(new_post), 201

@app.route('/post/<int:post_id>', methods=['DELETE'])
def delete_post(post_id):
    """
    Delete a post by ID
    ---
    parameters:
      - name: post_id
        in: path
        type: integer
        required: true
        description: The ID of the post to delete
    responses:
      200:
        description: Post deleted successfully
      404:
        description: Post not found
    """
    posts = read_posts()
    post = find_post_by_id(post_id)
    if not post:
        return jsonify({'error': 'Post not found'}), 404

    posts = [p for p in posts if p['id'] != post_id]
    write_posts(posts)
    return jsonify({'message': 'Post deleted successfully'}), 200

@app.route('/post/<int:post_id>', methods=['PUT'])
def edit_post(post_id):
    """
    Edit a post by ID
    ---
    parameters:
      - name: post_id
        in: path
        type: integer
        required: true
        description: The ID of the post to edit
      - name: body
        in: body
        required: true
        schema:
          type: object
          properties:
            title:
              type: string
            body:
              type: string
    responses:
      200:
        description: Post edited successfully
      400:
        description: Title or body must be provided
      404:
        description: Post not found
    """
    data = request.json
    title = data.get('title')
    body = data.get('body')

    if not title and not body:
        return jsonify({'error': 'Title or body must be provided'}), 400

    posts = read_posts()
    post = find_post_by_id(post_id)
    if not post:
        return jsonify({'error': 'Post not found'}), 404

    if title:
        post['title'] = title
    if body:
        post['body'] = body

    posts = [p if p['id'] != post_id else post for p in posts]
    write_posts(posts)
    return jsonify(post), 200

if __name__ == '__main__':
    app.run(debug=True)
